// Updated to work with jQuery 3.x.x

var price = 0;
var jQueryi = 0;
var jQueryname = "";

function formatNumber(_number, _cfg) {
    function thousands_sep(_num, _sep) {
        if (_num.length <= 3) return _num;
        var _count = _num.length;
        var _num_parser = '';
        var _count_digits = 0;
        for (var _p = (_count - 1); _p >= 0; _p--){
        var _num_digit = _num.substr(_p, 1);
        if (_count_digits % 3 == 0 && _count_digits != 0 && !isNaN(parseFloat(_num_digit))) _num_parser = _sep + _num_parser;
        _num_parser = _num_digit + _num_parser;
        _count_digits++;
        }
        return _num_parser;
    }

    if (typeof _number !== 'number'){
        _number = parseFloat(_number);
        if (isNaN(_number)) return false;
    }

    var _cfg_default = {before: '', after: '', decimals: 2, dec_point: '.', thousands_sep: ','};

    if (_cfg && typeof _cfg === 'object'){
        _cfg = Object.assign({}, _cfg_default, _cfg);
    } else {
        _cfg = _cfg_default; 
    }

    _number = _number.toFixed(_cfg.decimals);

    if (_number.indexOf('.') != -1){
        var _number_arr = _number.split('.');
        var _number = thousands_sep(_number_arr[0], _cfg.thousands_sep) + _cfg.dec_point + _number_arr[1];
    } else {
        var _number = thousands_sep(_number, _cfg.thousands_sep);
    }

    return _cfg.before + _number + _cfg.after;
}

// Функция чтения из xml данных о производителях и цветах камня
function xmlParser(xml) {
    jQuery("#load").fadeOut();
    jQuery(".window").append('<button type="button" class="close">Х</button>');

    jQuery('.close').on("click", function (e) {
        e.preventDefault();
        jQuery('#mask, .window').hide();
    });

    jQuery(xml).find(jQueryname).find("stone").each(function () {
        jQuery(".window").append('<div class="preview" id="'+jQueryi+'"><img width="120" name="' + jQuery(this).find("name").text() + '" height="120" class="clos'+jQueryi+'" src="/wp-content/themes/kamber/calkulator/images/'+ jQueryname +'/resize/' + jQuery(this).find("image").text() + '" id="'+ jQuery(this).find("priceId").text() +'"></img><span>'+ jQuery(this).find("name").text() +'</span></div>');
        jQuery(".book").fadeIn(800);
        
        jQuery('.clos'+jQueryi+'').on("click", function() {
            var $this = jQuery(this);

            var id = $this.attr('id');
            var src = $this.attr('src');
            var name = $this.attr('name');

            jQuery('.materials').append(
                '<div class="material">' +
                '<label class="rc_line">' +
                '<div class="mat_image">' +
                '<img class="img' + jQueryi + '" width="120" height="120" />' +
                '</div>' +
                '<input name="radioColorStone" class="radio'+jQueryi+'" type="radio" checked="checked" value="" />' +
                '<span>' + $this.attr("name") + '</span>' +
                '</label>' +
                '</div>'
            );
            
            jQuery('.radio'+jQueryi+'').attr({
                id: name,
                value: id
            }).css("cursor","pointer");
            
            jQuery('.img'+ jQueryi +'').attr({ id, src, name }).css("cursor","pointer");
                
            jQuery(document).on("click", '.img'+ jQueryi +'', function() {
                $this.next().next().attr({"checked": "checked"});
            });

            jQuery('#mask, .window').hide();
        });
        
        jQuery('.close').on("click", function (e) {
    		e.preventDefault();
    		jQuery('#mask, .window').hide();
    	});
        
        jQueryi++;
    });
}

jQuery.exists = function(selector) {
    return (jQuery(selector).length > 0);
}
 
Array.prototype.sum = function() {
    for (var i=0, sum=""; i < this.length; sum += this[i++]);
    return sum;
}

jQuery(function($) {    
    // Скрываем толщину 40 мм
    jQuery('.cromki40').hide();

    // Скрываем метраж плинтусов
    jQuery('.pl-displayZ').hide();
    jQuery('.pl-displayA').hide();

    jQuery('body').append('<div id="boxes"><div id="dialog" class="window"></div><div id="mask"></div></div>');

    jQuery('a[name=modal]').on("click", function(e) {
        e.preventDefault();
        
        var $window = jQuery(window)
        var $mask = jQuery('#mask')
        var $this = jQuery(this)
        var $id = jQuery($this.attr('href'))

		var maskWidth = $window.width()*2;
		var maskHeight = $window.height()*2;
		var windowWidth = $window.width();
        var windowHeight = $window.height();
        
		$mask.css({
            width: maskWidth,
            height: maskHeight
        })
            .fadeIn(400)
            .fadeTo("slow",0.9);

		$id.css({
            top: (windowHeight - $id.height()) / 2,
            left: (windowWidth - $id.width()) / 2
        }).fadeIn(800);
        
        $mask.on("click", function () {
    		jQuery(this).hide();
    		jQuery('.window').hide();
    	});
        
        jQuery("#print").on("click", function() {
			jQuery('#dialog, .window').printThis({
			    loadCSS: "",
			    header: "<h3>Каменный Берег. Телефоны: 642-37-76, 642-57-76.<br>Сайт: mramor-granit-kvarz.ru Почта: kamenbereg@yandex.ru</h3>"
			});
        });
        
        jQuery('.close').on("click", function (e) {
    		e.preventDefault();
    		jQuery('#mask, .window').hide();
    	});
    });
    
    jQuery('input[name=radioTiknessTabel]').on("click", function() {
        if (jQuery(this).val() == 40) {
            jQuery('.cromki').hide();
            jQuery('.cromki40').show();
        } else {
            jQuery('.cromki').show();
            jQuery('.cromki40').hide();
        }
    });
	
	jQuery('input[name=p]').on("click", function() {
        if (jQuery(this).val() == 2) {
            jQuery('.pl-displayA').hide();
            jQuery('.pl-displayZ').show();
        } else {
            jQuery('.pl-displayA').show();
            jQuery('.pl-displayZ').hide();
        }
    });

    jQuery('input[name=radioFormTabel]').on("click", function() {
        var html = '';

        if (jQuery(this).val() == 3) {
            html = 
                "<div class='three'>"+
                "<div class='topsize1'>"+"<input type='text' name='size1' id='size1' maxlength='4' value='0'/>"+"</div>"+
                "<div class='topsize2'>"+"<input type='text' name='size2' id='size2' maxlength='4' value='0'/>"+"</div>"+
                "<div class='topsize3'>"+"<input type='text' name='size3' id='size3' maxlength='4' value='0'/>"+"</div>"+
                "<div class='topsize5'>"+"<input type='text' name='size7' id='size5' maxlength='4' value='0'/>"+"</div>"+
                "<div class='topsize6'>"+"<input type='text' name='size6' id='size4' maxlength='4' value='0'/>"+"</div>"+
                "<label><input type='checkbox' name='check' class='check0' value='0'/><span></span></label>"+
                "<label><input type='checkbox' name='check' class='check1' value='0'/><span></span></label>"+
                "<label><input type='checkbox' name='check' class='check2' value='0'/><span></span></label>"+
                "<label><input type='checkbox' name='check' class='check3' value='0'/><span></span></label>"+
                "<label><input type='checkbox' name='check' class='check4' value='0'/><span></span></label>"+
                "<label><input type='checkbox' name='check' class='check5' value='0'/><span></span></label>"+
                "<label><input type='checkbox' name='check' class='check6' value='0'/><span></span></label>"+
                "<label><input type='checkbox' name='check' class='check7' value='0'/><span></span></label>"+
                "</div>";
        } else if (jQuery(this).val() == 2) {
            html = 
                "<div class='two'>"+
                "<div class='topsize1'>"+"<input type='text' name='size1' id='size1' maxlength='4' value='0'/>"+"</div>"+
                "<div class='topsize2'>"+"<input type='text' name='size2' id='size2' maxlength='4' value='0'/>"+"</div>"+
                "<div class='topsize3'>"+"<input type='text' name='size3' id='size3' maxlength='4' value='0'/>"+"</div>"+
                "<div class='topsize4'>"+"<input type='text' name='size4' id='size4' maxlength='4' value='0'/>"+"</div>"+
                "<label><input type='checkbox' name='check' class='check0' value='0'/><span></span></label>"+
                "<label><input type='checkbox' name='check' class='check1' value='0'/><span></span></label>"+
                "<label><input type='checkbox' name='check' class='check2' value='0'/><span></span></label>"+
                "<label><input type='checkbox' name='check' class='check3' value='0'/><span></span></label>"+
                "<label><input type='checkbox' name='check' class='check4' value='0'/><span></span></label>"+
                "<label><input type='checkbox' name='check' class='check5' value='0'/><span></span></label>"+
                "</div>";
        } else if (jQuery(this).val() == 1) {
            html =
                "<div class='one'>"+
                "<div class='topsize1'>"+"<input type='text' name='size1' id='size1' maxlength='4' value='0'/>"+"</div>"+
                "<div class='topsize2'>"+"<input type='text' name='size2' id='size2' maxlength='4' value='0'/>"+"</div>"+
                "<label><input type='checkbox' name='check' class='check0' value='0'/><span></span></label>"+
                "<label><input type='checkbox' name='check' class='check1' value='0'/><span></span></label>"+
                "<label><input type='checkbox' name='check' class='check2' value='0'/><span></span></label>"+
                "<label><input type='checkbox' name='check' class='check3' value='0'/><span></span></label>"+
                "</div>";
        }

        jQuery('.tabletop_wrap').empty().append(html);

        jQuery(".container_theme input[type=text]").on("focusout", function() {
            jQuery(this).attr("value", jQuery(this).val() || 0);
        });
            
        jQuery(".container_theme input[type=text]").on("click", function() {
           jQuery(this).attr("value", ""); 
        });
    }).trigger('click');
        
    // XML
    jQuery('.vivod').on("click", function() {
        jQuery(".window").empty();
        jQueryname = jQuery(this).attr('id');
        jQuery.ajax({
            type: "GET",
            url: "/wp-content/themes/kamber/calkulator/data.xml",
            dataType: "xml",
            success: xmlParser
        });
    });

    // END XML

    jQuery(".edge_list img").on("click", function() {
        jQuery(this).next().next().attr({"checked": "checked"}); 
    });
    
    jQuery(".elements_list img").on("click", function() {
        if (jQuery(this).next().next().prop("checked")) {
            jQuery(this).next().next().removeAttr("checked"); 
        } else {
            jQuery(this).next().next().attr({"checked": "checked"});
        }
    });
	
	jQuery(".width_list img").on("click", function() {
        jQuery(this).next().next().attr({"checked": "checked"}); 
    });
	
	jQuery(".material img").on("click", function() {
        jQuery(this).next().attr("checked","checked"); 
    });

    jQuery('#result').on("click", function() {
        if (jQuery.exists(".two")) {
            if (jQuery("#size1").val() == 0 || jQuery("#size2").val() == 0 || jQuery("#size3").val() == 0 || jQuery("#size4").val() == 0) {
                alert("Пожалуйста, введите размеры Вашего изделия в сантиметрах.");
            } else {
                var S = jQuery("#size1").val()*jQuery("#size4").val() + (jQuery("#size2").val()-jQuery("#size4").val())*jQuery("#size3").val();
                var lenghtCromki=[];
				var pogonMetr = ((parseInt(jQuery("#size1").val()) + parseInt(jQuery("#size2").val()))-60)/100;
				var postRasxod = 7140;
                var kolPila = ((parseInt(jQuery("#size1").val()) + parseInt(jQuery("#size4").val())) + (parseInt(jQuery("#size2").val()) + parseInt(jQuery("#size3").val())) + (parseInt(jQuery("#size2").val()) - parseInt(jQuery("#size4").val())) + (parseInt(jQuery("#size1").val()) - parseInt(jQuery("#size3").val()))) / 100;
                var sum = 0;
				var pricePil = kolPila * 352;
   
                for (var i = 0; i < 6; i++) {
                    if (jQuery(".check"+i).prop("checked")) {
                        lenghtCromki[lenghtCromki.length] = jQuery(".check"+i+":checked").attr("class");
                    } 
                }
                
                for (var i = 0; i < lenghtCromki.length; i++) {
                    if (lenghtCromki[i] == "check0") {
                        sum += parseInt(jQuery("#size1").val()); 
                    }
                    if (lenghtCromki[i] == "check1") {
                        sum += parseInt(jQuery("#size2").val());
                    }
                    if (lenghtCromki[i] == "check2") {
                        sum += parseInt(jQuery("#size3").val()); 
                    }
                    if (lenghtCromki[i] == "check3") {
                        sum += parseInt((jQuery("#size2").val()-jQuery("#size4").val())); 
                    }
                    if (lenghtCromki[i] == "check4") {
                        sum += parseInt((jQuery("#size1").val()-jQuery("#size3").val())); 
                    }
                    if (lenghtCromki[i] == "check5") {
                        sum += parseInt(jQuery("#size4").val()); 
                    }
                }   
            }
        } else if (jQuery.exists(".one")) {
            if (jQuery("#size1").val() == 0 || jQuery("#size2").val() == 0) {
                alert("Вы ввели не все размеры!");
            } else {
                var S = jQuery("#size1").val() * jQuery("#size2").val();
                var lenghtCromki=[];
                var kolPila = (parseInt(jQuery("#size1").val()) + parseInt(jQuery("#size2").val())) / 100 * 2;
				var pricePil = kolPila * 352;
				var pogonMetr = parseInt(jQuery("#size1").val()) / 100;
				var postRasxod = 7140;
                var sum = 0;

                for (var i = 0; i < 4; i++) {
                    if (jQuery(".check"+i).prop("checked")) {
                        lenghtCromki[lenghtCromki.length]= jQuery(".check"+i+":checked").attr("class");
                    } 
                }
                
                for (var i =0; i < lenghtCromki.length; i++) {
                    if (lenghtCromki[i] == "check0") {
                        sum += parseInt(jQuery("#size1").val()); 
                    }
                    if (lenghtCromki[i] == "check1") {
                        sum += parseInt(jQuery("#size2").val());
                    }
                    if (lenghtCromki[i] == "check2") {
                        sum += parseInt(jQuery("#size1").val()); 
                    }
                    if (lenghtCromki[i] == "check3") {
                        sum += parseInt(jQuery("#size2").val()); 
                    }
                }  
            }
        } else if (jQuery.exists(".three")) {
            if (jQuery("#size1").val() == 0 || jQuery("#size2").val() == 0 || jQuery("#size3").val() == 0 || jQuery("#size4").val() == 0 || jQuery("#size5").val() == 0 || jQuery("#size6").val() == 0 || jQuery("#size7").val() == 0 ) {
                alert("Вы ввели не все размеры!");
            } else {
                var S = (jQuery("#size1").val()*60)+((jQuery("#size2").val()-60)*jQuery("#size3").val())+((jQuery("#size5").val()-60)*jQuery("#size4").val());
                var lenghtCromki=[];
                var pogonMetr = ((parseInt(jQuery("#size1").val()) + parseInt(jQuery("#size2").val()) + parseInt(jQuery("#size5").val()))-120) / 100;
				var postRasxod = 7140;
                var kolPila = ((parseInt(jQuery("#size1").val()) + parseInt(jQuery("#size2").val()) + parseInt(jQuery("#size3").val()) + parseInt(jQuery("#size4").val()) + parseInt(jQuery("#size5").val())) + ((parseInt(jQuery("#size2").val()) + parseInt(jQuery("#size5").val()))-120) + (parseInt(jQuery("#size1").val()) - (parseInt(jQuery("#size3").val()) + parseInt(jQuery("#size4").val())))) / 100;
				var pricePil = kolPila * 352;
                var sum = 0;

                for (var i = 0; i < 8; i++) {
                    if (jQuery(".check"+i).prop("checked")==true) {
                        lenghtCromki[lenghtCromki.length]= jQuery(".check"+i+":checked").attr("class");
                    } 
                }
                
                for (var i = 0; i < lenghtCromki.length; i++) {
                    if (lenghtCromki[i] == "check0") {
                        sum += parseInt(jQuery("#size1").val()); 
                    }
                    if (lenghtCromki[i] == "check1") {
                        sum += parseInt(jQuery("#size2").val());
                    }
                    if (lenghtCromki[i] == "check2") {
                        sum += parseInt(jQuery("#size3").val()); 
                    }
                    if (lenghtCromki[i] == "check3") {
                        sum += parseInt(jQuery("#size2").val()-60); 
                    }
                    if (lenghtCromki[i] == "check4") {
                        sum += parseInt(jQuery("#size1").val()-jQuery("#size3").val()-jQuery("#size4").val()); 
                    }
                    if (lenghtCromki[i] == "check5") {
                        sum += parseInt(jQuery("#size5").val()-60); 
                    }
                    if (lenghtCromki[i] == "check6") {
                        sum += parseInt(jQuery("#size4").val()); 
                    }
                    if (lenghtCromki[i] == "check7") {
                        sum += parseInt(jQuery("#size5").val()); 
                    }
                }
            }
        }

        var stoneColorPrises = {
            'dymovskij': 95 * dollar,
            'zheltau': 4500,
            'plazaOne': 150 * dollar,
            'plazaTwo': 158 * dollar,
            'plazaTree': 178 * dollar,
            'plazaFour': 190 * dollar,
            'caesarOne': 195 * euro,
            'caesarTwo': 200 * euro,
            'caesarTree': 205 * euro,
            'caesarFour': 233 * euro,
            'stillOne': 131 * dollar,
            'stillTwo': 167 * dollar,
            'stillTree': 191 * dollar,
            'stillFour': 215 * dollar,
            'techOne': 136 * euro,
            'techTwo': 145 * euro,
            'techTree': 150 * euro,
            'techFour': 157 * euro,
            'techFive': 175 * euro,
            'techSix': 188 * euro,
            'techSeven': 215 * euro,
            'smaOne': 140 * dollar,
            'smaTwo': 170 * dollar,
            'smaTree': 200 * dollar,
            'smaFour': 230 * dollar
        }

        var selectedStoneColor = Object.keys(stoneColorPrises).find(function(color) { return jQuery("[name=radioColorStone]:checked").val() === color })
        price = stoneColorPrises[selectedStoneColor]

		//-------------Подсчет вырезов--------------------
		
		//-------------Вид мойки--------------------
		if (jQuery("#sink1").prop("checked")) { var vidSink = "Накладная"; }
        if (jQuery("#sink2").prop("checked")) { var vidSink = "Подстольная"; }	
        
		//-------------else { var vidSink = ""};--------------------
		if (jQuery("[name=virezSink]:checked").length == 1) {        
			var stoimostSink = jQuery("[name=virezSink]:checked").val() * 1;
            var textSink = "<tr><td>Тип мойки:</td><td>"+vidSink+"</td></tr>";
        } else {
            var stoimostSink = 0;
            var textSink = "";
        }
		
        var stoimostPanel = 0;

        if (jQuery("[name=panel]").prop("checked")) {
            stoimostPanel += parseInt(jQuery("[name=panel]:checked").val());
            var textPanel = "<tr><td>Варочная панель:</td><td>Вырез под варочную панель</td></tr>";
        } else {
            var textPanel = "";
        }
		  
        var sverlo = jQuery("[name=sverlo15]").val()*278+
        jQuery("[name=sverlo35]").val()*418+
        jQuery("[name=sverlo65]").val()*506;
        
        if (jQuery("[name=sverlo15]").val() != 0) {
            var textSverlo15 = "<tr><td>Отверстия до 30 мм</td><td>"+jQuery("[name=sverlo15]").val()+" шт.</td></tr>";
        } else {
            var textSverlo15 = "";
        }
        
        if (jQuery("[name=sverlo35]").val() != 0) {
            var textSverlo35 = "<tr><td>Отверстия от 30 до 65 мм</td><td>"+jQuery("[name=sverlo35]").val()+" шт.</td></tr>";
        } else {
            var textSverlo35 = "";
        }
        
        if (jQuery("[name=sverlo65]").val() != 0) {
            var textSverlo65 = "<tr><td>Отверстия свыше 65 мм</td><td>"+jQuery("[name=sverlo65]").val()+" шт.</td></tr>";
        } else {
            var textSverlo65 = "";
        }
		
		//-------------Кол-во материала/0.9-0.8 - скидка 10-20%--------------------
		var kolMateriala = 0;
		if (pogonMetr <= 1.5) {
            kolMateriala += price*2.1*0.8; 
        }
		if (pogonMetr > 1.5 && pogonMetr <= 3) {
            kolMateriala += price*2.1; 
        }
		if (pogonMetr > 3 && pogonMetr <= 4.5) {
            kolMateriala += price*4.2*0.9; 
        }
		if (pogonMetr > 4.5 && pogonMetr <= 6) {
            kolMateriala += price*4.2; 
        }
		if (pogonMetr > 6  && pogonMetr <= 7.5) {
            kolMateriala += price*6.3*0.9; 
        }
		if (pogonMetr > 7.5 && pogonMetr <= 9) {
            kolMateriala += price*6.3; 
        }
		if (pogonMetr > 9 && pogonMetr <= 10.5) {
            kolMateriala += price*8.4*0.9; 
        }
		if (pogonMetr > 10.5 && pogonMetr <= 12) {
            kolMateriala += price*8.4; 
        }
		if (pogonMetr > 12 && pogonMetr <= 13.5) {
            kolMateriala += price*10.5*0.8; 
        }
		if (pogonMetr > 13.5 && pogonMetr <= 15) {
            kolMateriala += price*10.5; 
        }
						
		//------------Скругления----------------
        
        var scrugAngel = jQuery("[name=skrugAngel]").val() * 420;
        var scrugLecalAngel = jQuery("[name=skrugLecalAngel]").val() / 100 * 2200;

        if (jQuery("[name=skrugAngel]").val() != 0) {
            var textSkrugAngel = "<tr><td>Скругление углов:</td><td>"+jQuery("[name=skrugAngel]").val()+" шт.</td></tr>";
        } else { var textSkrugAngel = "" };
        
        if (jQuery("[name=skrugLecalAngel]").val() != 0) {
            var textSkrugLecalAngel = "<tr><td>Лекальные скругления:</td><td>"+jQuery("[name=skrugLecalAngel]").val()/100+" п.м.</td></tr>";
        } else { var textSkrugLecalAngel = "" };

        //-------------Вид кромки--------------------
        var edgeTypes = {
            "#A": "Кромка А",
            "#H": "Кромка H",
            "#T": "Кромка T",
            "#V": "Кромка V",
            "#Z": "Кромка Z",
            "#Q": "Кромка Q",
            "#ZZ": "Кромка Z+Z.",
            "#А15A10": "Кромка А15+A10",
            "#F5V": "Кромка F+5+V",
            "#A15T": "Кромка A15+T",
            "#H5L": "Кромка H+5+L",
            "#Z45": "Кромка Z45"
        }

        var vidCromki = edgeTypes[Object.keys(edgeTypes).find(function (edgeTypeId) { return jQuery(edgeTypeId).prop("checked"); })] || "";
		
		if (jQuery("[name=a]:checked").length == 1) {        
            var stoimostCromki = (sum/100)*jQuery("[name=a]:checked").val();
            var textCromki = "<tr><td>Длина видимого торца столешницы ("+vidCromki+"):</td><td>"+(sum/100)+" п.м.</td></tr>";
        } else {
            var stoimostCromki = 0;
            var textCromki = "";
        }
		
		//-------------Вид плинтуса--------------------
		
		if (jQuery("[name=p]:checked").val() == 2) { var vidPlintusa = "Кромка Z"; jQuery("[name=sizePlintusA]").val('');} 
		if (jQuery("[name=p]:checked").val() == 1) { var vidPlintusa = "Кромка A"; jQuery("[name=sizePlintusZ]").val('');}
		
		
		//-------------Стоимость плинтуса за погон--------------------
		if (jQuery("[name=p]:checked").length == 1) {
            var plintus = (jQuery("[name=sizePlintusA]").val() / 100 * 2030) + (jQuery("[name=sizePlintusZ]").val()/100*1760);
        } else {
            var plintus = 0;
        }  

        if (plintus != 0) {
            var textSizePlintus = "<tr><td>Длина плинтуса ("+vidPlintusa+"):</td><td>"+(jQuery("[name=sizePlintusA]").val()/100 +jQuery("[name=sizePlintusZ]").val()/100) +" п.м.</td></tr>";
        } else {
            var textSizePlintus = "";
        }
		
        //-------------Вывод результата--------------------
		
        var stoimost = 0;
		if (jQuery("[name=radioTiknessTabel]:checked").val() == 20 ){
            stoimost += pricePil + scrugAngel + scrugLecalAngel + stoimostCromki + kolMateriala + plintus + sverlo + postRasxod + (pogonMetr*2000) + stoimostSink + stoimostPanel; 
        }
		if (jQuery("[name=radioTiknessTabel]:checked").val() == 30 ){
            stoimost += (pricePil + scrugAngel + scrugLecalAngel + stoimostCromki + kolMateriala + plintus)*1.3 + sverlo + postRasxod + (pogonMetr*2000) + stoimostSink + stoimostPanel; 
        }  
		if (jQuery("[name=radioTiknessTabel]:checked").val() == 40 ){
            stoimost += ((pricePil + scrugAngel + scrugLecalAngel)*2) + stoimostCromki + kolMateriala + plintus + sverlo + postRasxod + (pogonMetr*2000) + stoimostSink + stoimostPanel + ((kolPila-(sum/100))*1050); 
        }

		//-------------Скидка за месяц--------------------
		var monthsale = 0;
		if (stoimost > 0) {
            monthsale += stoimost*0.9; 
        }
						
		//-------------Всплывающее окно--------------------
                
        jQuery(".window").empty().append(
            '<button type="button" class="close">X</button>' +
            '<img id="print" title="Распечатать" src="/wp-content/themes/kamber/calkulator/images/print.svg"/>' +
            '<div class="clr"></div>' +
            '<h1 class="title">Предварительная смета</h1>' +
            '<table><tr>' +
            '<td>Название материала:</td>' +
            '<td>' + jQuery("[name=radioColorStone]:checked").attr("id") + '</td>' +
            '</tr><tr>' +
            '<td>Площадь изделия:</td>' +
            '<td>' + (S / 10000) + ' кв.м.</td>' +
            '</tr><tr>' +
            '<td>Кол-во погоннных метров:</td>' +
            '<td>' + pogonMetr + ' м.п.</td>' +
            '</tr>' + textCromki + textSink + textPanel + textSverlo15 + textSverlo35 + textSverlo65 + textSkrugAngel + textSkrugLecalAngel + textSizePlintus + '<tr>' +
            '<td>Стоимость с установкой в пределах КАД за 10 рабочих дней:</td>' +
            '<td class="active">' + formatNumber(stoimost, {after: " коп.", thousands_sep: "", dec_point: " руб. "}) + '</td>' +
            '</tr><tr>' +
            '<td>С предварительным заказом столешницы за месяц:</td>' +
            '<td class="active">' + formatNumber(monthsale, {after: " коп.", thousands_sep: "", dec_point: " руб. "}) +
            '</td></tr></table>'
        );

    });
});
